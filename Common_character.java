import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Scanner;

class  Common_character
{
	public static void main(String args[])
	{
		LinkedHashSet<Character> t1 = new LinkedHashSet<Character>();
		LinkedHashSet<Character> t2 = new LinkedHashSet<Character>();
		StringBuilder sb = new StringBuilder();
		Scanner in = new Scanner(System.in);
		String s1 =in.nextLine().toUpperCase();
		String s2 =in.nextLine().toUpperCase();
		
		for(char c1:s1.toCharArray())
		{
			t1.add(c1);
		}
		
		for(char c2:s2.toCharArray())
		{
			t2.add(c2);
		}
		
		t2.retainAll(t1);
		
		Iterator  i = t2.iterator();
		
		while(i.hasNext())
		{
			sb.append(i.next());
		}
		
		System.out.print(sb);
	}
}